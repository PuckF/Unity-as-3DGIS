﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using Npgsql;
using System.Data;
using System.Data.Linq;
using System.Globalization;
using System.Text.RegularExpressions;
using UnityEngine.UI;

public class CreateAttributes : MonoBehaviour
{
    public Text AttributeText;
    [Tooltip("Contains the children of this GameObject")]
    [SerializeField]
    private List<GameObject> BuildingObjectList;
    public GameObject[] BuildingList = null;
    public Camera mainCam;
    private double Camera_x;
    private double Camera_z;
    // Use this for initialization
    void Start()
    {
        Camera_x = mainCam.transform.position.x;
        Camera_z = mainCam.transform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        //Update list of buildings when new buildings are loaded. 
        if (BuildingList.Length == 0 || (Math.Abs(Camera_x - mainCam.transform.position.x) > 200 || (Math.Abs(Camera_z - mainCam.transform.position.z) > 200)))
        {
            Camera_z = mainCam.transform.position.z;
            Camera_x = mainCam.transform.position.x;
            BuildingList = GameObject.FindGameObjectsWithTag("building");
            foreach (GameObject building in BuildingList)
            {
                building.AddComponent<MeshCollider>();
            }
        }
        //Get building ID that is clicked
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                //Reset color of all buildings to white
                foreach (GameObject resetBuilding in BuildingList)
                {
                    resetBuilding.GetComponent<Renderer>().material.color = Color.white;
                }
                //Get clicked building name, print attribute info on screen, and color green
                GameObject clickedBuilding = hit.transform.gameObject;
                clickedBuilding.GetComponent<Renderer>().material.color = Color.green;
                print(hit.transform.name);
                PrintInfo(hit.transform.name);

            }
            else
            { /* handled in button
                foreach (GameObject resetBuilding in BuildingObjectList)
                {
                    resetBuilding.GetComponent<Renderer>().material.color = Color.white;
                    string TextSetter = "";
                    AttributeText.text = TextSetter;
                }*/
            }
        }
    }
    void PrintInfo(string InputName)
    {
        string prefix = "building "; //Edit this string to define the prefix of the object names
        string buildingID = InputName.Replace(prefix, "");
        int buildingINT = Convert.ToInt32(buildingID);
        try
        {
            // Connect to a PostgreSQL database
            NpgsqlConnection conn = new NpgsqlConnection("Server=127.0.0.1;User Id=postgres; " +
               "Password=abc123;Database=dbname;");
            conn.Open();
            //Edit connectionstring to obtain building info
            string ConnectionString = string.Format("SELECT objectid, hoogte, hoogteklasse, type_gebouw FROM \"gebouw_3d_lod1\" WHERE objectid ={0};", buildingINT);
            NpgsqlCommand command = new NpgsqlCommand(ConnectionString, conn); 
            NpgsqlDataReader dr = command.ExecuteReader();
            // Output rows
            int objectid = new int();
            float hoogte = new float();
            string hoogteklasse = null;
            string type_gebouw = null;
            while (dr.Read())
            {

                objectid = (int)dr[0];
                hoogte = (float)dr[1];
                hoogteklasse = (string)dr[2];
                type_gebouw = (string)dr[3];
            }
            string TextSetter = string.Format("Object ID: {0} \nHoogte: {1:F1} m \nHoogteklasse: {2} \nType gebouw: {3}", objectid, hoogte, hoogteklasse, type_gebouw);
            AttributeText.text = TextSetter;
            conn.Close();
        }
        catch {
            string TextSetter = "Could not retreive information";
            AttributeText.text = TextSetter;
        }
    }
}
