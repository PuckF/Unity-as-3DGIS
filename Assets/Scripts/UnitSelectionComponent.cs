﻿//Rectangle box selection based on https://hyunkell.com/blog/rts-style-unit-selection-in-unity-5/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using Npgsql;
using System.Data;
using System.Data.Linq;
using System.Globalization;
using System.Text.RegularExpressions;
using UnityEngine.UI;

public class UnitSelectionComponent : MonoBehaviour
{
    bool isSelecting = false;
    bool ctrlDown = false; 
    Vector3 mousePosition1;
    public List<string> SelectedBuildings= new List<string>();
    public GameObject[] BuildingList = null;
    public Camera mainCam;
    private double Camera_x;
    private double Camera_z;

    private void Start()
    {
        Camera_x = mainCam.transform.position.x;
        Camera_z = mainCam.transform.position.z;
    }
    void Update()
    {
        //Put new buildings in buildinglist when updated
        if (BuildingList.Length == 0 || (Math.Abs(Camera_x - mainCam.transform.position.x) > 200 || (Math.Abs(Camera_z - mainCam.transform.position.z) > 200)))
        {
            BuildingList = GameObject.FindGameObjectsWithTag("building");
            Camera_x = mainCam.transform.position.x;
            Camera_z = mainCam.transform.position.z;
        }
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            ctrlDown = true;
        }
        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            ctrlDown = false;
        }
        //This part will make sure that a box selection can be done, while holding ctrl+left mouse button
        // If we press the left mouse button+CTRL, save mouse location and begin selection
        if (ctrlDown)
        {
            if (Input.GetMouseButtonDown(0))
            {
                isSelecting = true;
                mousePosition1 = Input.mousePosition;
            }
            // If we let go of the left mouse button, end selection
            if (Input.GetMouseButtonUp(0))
            {
                foreach (GameObject building in BuildingList)
                {
                    if (IsWithinSelectionBounds(building.gameObject))
                    {
                        print("inbounds");
                        SelectedBuildings.Add(building.name);
                        building.GetComponent<Renderer>().material.color = Color.green;
                    }
                    else
                    {
                        building.GetComponent<Renderer>().material.color = Color.white;
                    }
                }
                Debug.Log(SelectedBuildings);
                isSelecting = false;
            }
            
        }
    }
    void OnGUI()
    {
        //Gives visual feedback of box drawn when selecting
        if (isSelecting)
        {
            // Create a rect from both mouse positions
            var rect = Utils.GetScreenRect(mousePosition1, Input.mousePosition);
            Utils.DrawScreenRect(rect, new Color(0.8f, 0.8f, 0.95f, 0.25f));
            Utils.DrawScreenRectBorder(rect, 2, new Color(0.8f, 0.8f, 0.95f));
        }
    }
    public bool IsWithinSelectionBounds(GameObject gameObject)
    {
        //Returns list of objects in the selection rectangle
        if (!isSelecting)
            return false;
        var camera = Camera.main;
        var viewportBounds =
            Utils.GetViewportBounds(camera, mousePosition1, Input.mousePosition);
        return viewportBounds.Contains(
            camera.WorldToViewportPoint(gameObject.GetComponent<MeshFilter>().mesh.bounds.center));

    }

}