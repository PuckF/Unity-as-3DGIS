﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using Npgsql;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Globalization;
using System.Text.RegularExpressions;
using UnityEngine.UI;

public class LayerToggle : MonoBehaviour
{
    //If there are more layers, edit this script
    public GameObject[] BuildingList;
    public Transform TerrainList;
    public Camera mainCam;
    private double Camera_x;
    private double Camera_z;
    private void Start()
    {
        Camera_x = mainCam.transform.position.x;
        Camera_z = mainCam.transform.position.z;
    }
    private void Update()
    {
        if (BuildingList.Length == 0 || (Math.Abs(Camera_x - mainCam.transform.position.x) > 200 || (Math.Abs(Camera_z - mainCam.transform.position.z) > 200)))
        {
            BuildingList = GameObject.FindGameObjectsWithTag("building");
            Camera_x = mainCam.transform.position.x;
            Camera_z = mainCam.transform.position.z;
        }
    }
    public void BuildingToggle()
    {
        foreach (GameObject building in BuildingList)
        {
            if (building.activeSelf)
            {
                building.SetActive(false);
            }
            else
            {
                building.SetActive(true);
            }


        }
    }
    public void TerrainToggle()
    {
        foreach (Transform child in TerrainList)
        {
            foreach (Transform subchild in child)
            {
                if (subchild.gameObject.activeSelf)
                {
                    subchild.gameObject.SetActive(false);
                }
                else
                {
                    subchild.gameObject.SetActive(true);
                }
            }
        }
    }
}
