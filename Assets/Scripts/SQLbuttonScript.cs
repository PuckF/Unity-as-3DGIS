﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using Npgsql;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Globalization;
using System.Text.RegularExpressions;
using UnityEngine.UI;

public class SQLbuttonScript : MonoBehaviour
{
    public GameObject[] BuildingList;
    public InputField SQLfield;
    public Text AttributeText;
    List<string> SelectedList = new List<string>();
    public Camera mainCam;
    private double Camera_x;
    private double Camera_z;
    // Use this for initialization
    private void Start()
    {
        Camera_x = mainCam.transform.position.x;
        Camera_z = mainCam.transform.position.z;
    }
    private void Update()
    {
        if (BuildingList.Length == 0 || (Math.Abs(Camera_x - mainCam.transform.position.x) > 200 || (Math.Abs(Camera_z - mainCam.transform.position.z) > 200)))
        {
            BuildingList = GameObject.FindGameObjectsWithTag("building");
            Camera_x = mainCam.transform.position.x;
            Camera_z = mainCam.transform.position.z;
        }
        
    }
    public void SQLclick()
    {
        //Get WHERE input for SQL query from the input
        string SQLtext = SQLfield.text;
        SelectionOperation(SQLtext);

    }
    void SelectionOperation(string criterium)
    {

        //print("Establishing connection");
        // Connect to a PostgreSQL database
        NpgsqlConnection conn = new NpgsqlConnection("Server=127.0.0.1;User Id=postgres; " +
            "Password=abc123;Database=dbname;");
        conn.Open();

        //Retreive all building IDs where the criterium is correct
        string ConnectionString = string.Format("SELECT objectid FROM \"gebouw_3d_lod1\" WHERE {0};", criterium);
        NpgsqlCommand command = new NpgsqlCommand(ConnectionString, conn); //
        NpgsqlDataReader dr = command.ExecuteReader();
        
        List<int> QueryList = new List<int>();
        while (dr.Read())
        {
            QueryList.Add((int)dr[0]);
        }
        conn.Close();
        SelectBuildings(QueryList);
    }
    void SelectBuildings(List<int> BuildingIDlist)
    {
        //Get a list of selected buildings and Color all selected buildings Green
        SelectedList.Clear();
        foreach (int BuildingID in BuildingIDlist)
        {
            string buildingName = string.Format("building {0}", BuildingID);
            SelectedList.Add(buildingName);
        }
        
        foreach (GameObject building in BuildingList)
        {
            if (SelectedList.Contains(building.name))
            {
                
                GameObject ChildObject = building.gameObject;
                ChildObject.GetComponent<Renderer>().material.color = Color.green;
            }
            //print(child.name);
        }
    }

    public void ClearSelection()
    {
        //When clearselection is pressed, remove objects from selection
        foreach (GameObject resetBuilding in BuildingList)
        {
            GameObject ChildObject = resetBuilding.gameObject;
            ChildObject.GetComponent<Renderer>().material.color = Color.white;
            string TextSetter = "";
            AttributeText.text = TextSetter;
        }
    }
    public void ExportSelection()
    {
        //make a new DB with selected buildings only
        SelectedList.Clear();
        foreach (GameObject building in BuildingList)
        {
            GameObject ChildObject = building.gameObject;
            Color ObjectColor = ChildObject.GetComponent<Renderer>().material.color;
            if (ObjectColor == Color.green){
                SelectedList.Add(building.name);
            }
        }
        print(SelectedList.Count);
        List<int> IDList = new List<int>();
        foreach (string buildingName in SelectedList)
        {
            print(buildingName); 
            string prefix = "building "; //Edit this string to define the prefix of the object names
            string buildingID = buildingName.Replace(prefix, "");
            int buildingINT = Convert.ToInt32(buildingID);
            IDList.Add(buildingINT);
        }
        ExportSQL(IDList);
    }
    void ExportSQL(List<int> IDList)
    {
        // Connect to a PostgreSQL database
        NpgsqlConnection conn = new NpgsqlConnection("Server=127.0.0.1;User Id=postgres; " +
           "Password=abc123;Database=dbname;");
        conn.Open();
        string DropTable = "DROP TABLE IF EXISTS ExportSelection;";
        NpgsqlCommand command = new NpgsqlCommand(DropTable, conn);
        command.ExecuteNonQuery();
        
        string ObjectSelection = IDlistformat(IDList);

        string ConnectionString = string.Format("CREATE TABLE ExportSelection AS SELECT * FROM \"gebouw_3d_lod1\" WHERE objectid in {0};", ObjectSelection);
        print(ConnectionString);
        NpgsqlCommand command2 = new NpgsqlCommand(ConnectionString, conn); //
        command2.ExecuteNonQuery();
        conn.Close();
        
    }
    string IDlistformat(List<int> IDList)
    {
        string SB = string.Format("({0}", IDList[0]);
        foreach (int StringID in IDList.Skip(1))
        {
            SB = SB + ", " + StringID.ToString();
        }
        SB = SB + ")";
        return SB;
    }
}

